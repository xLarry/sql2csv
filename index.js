var _					= require('underscore')
	, fs				= require('fs')
	, json2csv	= require('json2csv')
	, mssql			= require('node-sqlserver-unofficial')
	, Sequelize = require('sequelize');

var sequelize = new Sequelize('sqlite://chinook.db');
	, connStr		= 'Driver={SQL Server Native Client 11.0};Server=myServerAddress;Database=myDatabase;Trusted_Connection=True;'
	, fields		= ['CustomerId', 'FirstName', 'LastName'];
	, command		= fs.readFileSync("command.sql", "utf-8");

sequelize
	.query(command, {type: sequelize.QueryTypes.SELECT})
	.then( (result) => {
		
		var groupedResults = _.groupBy(result, (row) => row.Country + ';' + row.City);

		for (var group in groupedResults) {
			if (!groupedResults.hasOwnProperty(group)) continue;

			json2csv({ data: groupedResults[group], del: ';', quotes: '', hasCSVColumnTitle: false }, (err, csv) => {
				fs.writeFile('output-' + group + '.csv', csv, (err) => console.log('file saved'))
			});	
		}
	});

	// mssql.query(connStr, command, function(err, result) {
	// 	if(err)
	// 		res.end("Query Failed \n" + err);
	// 	else
	// 		res.end("Query result: " + result[0]['Column0'] + " \n");
	// });